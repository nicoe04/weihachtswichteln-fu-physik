import numpy as np
from sympy.combinatorics import Permutation
import json

def find_permuation(number_new_wichtel: int):
    
    # Generate a permutation
    
    cycles = 0  
    while cycles != 1:
        
        permutation = Permutation.random(number_new_wichtel)
    
        # Number of cycles in the permutation should be 1
        # This ensures, that everbody is included in one big string of giving packages
        # So that, not two people gift packages directly to each other
        cycles = permutation.cycles
    
    return permutation


new_wichtel = [{"email":"rudolf.xmas@fu-berlin.de", "info": "Ich esse keine Nüsse."},
                {"email":"santa.claus@fu-berlin.de", "info": "I am vegan."},
                {"email":"mrs.claus@fu-berlin.de", "info": "Ich lebe halal."},
                {"email":"frosty.snowman@fu-berlin.de", "info": ""},
                {"email":"grinch@fu-berlin.de", "info": "Ich bin vegan."},
                {"email":"elf@fu-berlin.de", "info": "Bitte schenk mir kein Essen"}]

# Load database of wichtel already in the game

try:
    with open('wichtel-database.json', 'r') as db_file:
        known_wichtel = json.load(db_file)
        
        number_known_wichtel = len(known_wichtel)
        
        # Not include wichtel with known email // remove duplicate emails
        
        known_emails = [wichtel["email"] for wichtel in known_wichtel]
        new_wichtel = [wichtel for wichtel in new_wichtel if wichtel["email"] not in known_emails]
        
except FileNotFoundError:
    # Create an empty database
    
    known_wichtel = []
    number_known_wichtel = 1

# Calculate permutation and assign recipients to the wichtel ----------------------

new_permutation = find_permuation(len(new_wichtel))

for i in range(len((new_wichtel))):
    
    # Find recipient using permutation
    
    recipient_nr = int(new_permutation.apply(i))
    
    new_wichtel[i].update({"my_nr": i + number_known_wichtel,
                      "recipient_nr": recipient_nr + number_known_wichtel,
                      "recipient_info": new_wichtel[recipient_nr]["info"],
                      "email_sent": False})

known_wichtel += new_wichtel

# Update the database ---------------------

with open('wichtel-database.json', 'w') as db_file:
    json.dump(known_wichtel, db_file)